**9. Konfiguration**

9.1. Schreibe den Code so, dass sich möglichst viel konfigurieren lässt

9.2. Halte dich an die [Vorgabe](../doc.md) für Konfigurationsdateien

9.3. Dokumentiere die neuen Konfigurationsparameter in den jeweiligen md-Dateien
