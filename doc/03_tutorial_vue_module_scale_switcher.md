
## Tutorial: Einen Vue basiertes Scale-Switcher-Tool erstellen
Eine Schritt für Schritt Dokumentation zur Erstellung eines Vue basierten Tools.
Ein vollständiges und lauffähiges Beispiel gibt es unter: https://bitbucket.org/illD/masterportal-vue/src/dev/


### Beispiel Anforderung
Wir wollen ein Tool schreiben, über welches man den Kartenmaßstab steuern kann. 
Dabei soll über ein Drop-Down-Menü der Maßstab ausgewählt werden. Sobald der Maßstab gesetzt wurde, soll sich die Karte anpassen.
Darüber hinaus soll unser Tool auf Änderungen des Kartenmaßstabes reagieren und den entsprechend aktuellen Maßstab im Drop-Down-Menu anzeigen.
Die hierbei verwendeten Technologien sind die folgenden:

* vuejs: https://vuejs.org/
* vuex: https://vuex.vuejs.org/guide/
* singleFileComponents: https://vuejs.org/v2/guide/single-file-components.html#ad 


### Dateiübersicht für das Anlegen eines neuen Tools
Da dieses Tutorial in die generelle Verwendung von Vue Komponenten im Kontext einer Backbone Anwendung einführen soll, werden alle Komponenten in einem Order organisiert. 
Die spätere notwendige Integration der Vue Komponenten im Kontext der Gesamtanwendung soll an diesem Punkt nicht behandelt werden.
Am Ende dieses Tutorials sollten alle aufgelisteten Dateien aneglegt worden sein

```
-  modules
   | -> vue-example
   |    |-> renderer
   |    |    | -> BackboneVueRenderer.js
   |    |-> scale-switch-menu
   |    |    | -> model.js
   |    |    | -> view.js
   |    |-> vue-files
   |    |    | -> NestedComponentTest.vue
   |    |    | -> ScaleSwitcher.vue
   |    |-> vuex-store
   |    |    | -> index.js
   |    |    | -> tools.module.js
```


### Notwendige Bibliotheken einbinden/installieren
vue.js - die für dieses Tutorial verwendete Version ist 2.6.10
```
npm install vue --save 
(oder mit Versionsnummer npm install vue@2.6.10 --save)
```

vue-loader - die für dieses Tutorial verwendete Version ist 15.7.1
```
npm install vue-loader --save 
(oder mit Versionsnummer npm install vue-loader@15.7.1 --save)
```

vue-template-compiler - die für dieses Tutorial verwendete Version ist 2.6.10
```
npm install vue-template-compiler --save 
(oder mit Versionsnummer npm install vue-template-compiler@2.6.10 --save)
```


### Die DevTools anpassen um Vue Dateien zu laden
In der Datei devtools/webpack.common.js folgende Zeilen hinzufügen
In den Imports am Dateianfang folgenden Code
```
VueLoaderPlugin = require('vue-loader/lib/plugin'),
```

In den Module Rules folgenden Code
```
{
    test: /\.vue$/,
    use: {
        loader: 'vue-loader'
    }
},
```

In den Plugins am Ende der Datei
```
new VueLoaderPlugin(),
```


### Scale-Switcher-Menu View erstellen
Datei *modules/vue-example/scale-switcher-menu/view.js* erstellen und der Datei den folgenden Code hinzufügen.

```js
const ScaleView = Backbone.View.extend({
    initialize: function () {
        this.listenTo(this.model, {
            "change:isActive": this.render
        });
        if (this.model.get("isActive") === true) {
            this.render(this.model, true);
        }
    },

    render: function (model, value) {
    }
});

export default ScaleView;
```


### Scale-Switcher-Menu Model erstellen und zurückgeben
Datei *modules/vue-example/scale-switcher-menu/model.js* erstellen und der Datei den folgenden Code hinzufügen.

```js
import Tool from "../../core/modelList/tool/model";

const ScaleModel = Tool.extend({
    defaults: _.extend({}, Tool.prototype.defaults, {
        glyphicon: "glyphicon-resize-full",
        renderToWindow: true
    }),
    initialize: function () {
        this.superInitialize();
    }
});

export default ScaleModel;
```


### Scale View initialisieren
In die datei *js/app.js* wechseln, Scale View importieren und initialiseren. 
Darauf achten, dass das Tool grundsätzlich erst nach dem Core initialsiert werden (dies gilt für jedes Modul).
Das Initialsieren eines Tools erfolgt als switch-case Anweisung innerhalb einer vorgesehenen _.each Schleife.

```js
// View importieren
import ScaleView from "../modules/vue-example/scale-switcher-menu/view";
// View initialsieren
  _.each(Radio.request("ModelList", "getModelsByAttributes", {type: "tool"}), function (tool) {
        switch (tool.id) {
          case "scale": {
                new ScaleView({model: tool});
                break;
          }
          ... // weitere Tools
      }
  });
```


## Model der View zuweisen
In der ModelList *modules/core/modelList/list.js* wird das Model importiert und in dem vorgesehenen if-else Statement per new ScaleModel() instanziiert.

```js
import ScaleModel from "../../vue-example/scale-switcher-menu/model";

 else if (attrs.type === "tool") {
    else if (attrs.id === "scale") {
         return new ScaleModel(attrs, options);
    }
    ... // weitere Tool-Models
 }
```


### Vuex-Store erstellen Teil 1
Datei *modules/vue-example/vuex-store/index.js* erstellen und der Datei den folgenden Code hinzufügen.

```js
import Vue from "vue";
import Vuex from "vuex";
import tools from "./tools.module";

Vue.use(Vuex);

export default new Vuex.Store({
    /*
    *   More modules to follow. E.g. auth, gfi ...
    *   Good example for organising the vuex: https://github.com/gothinkster/vue-realworld-example-app/tree/master/src/store
    */
    modules: {
        tools
    }
});
```


### Vuex-Store erstellen Teil 2
Datei *modules/vue-example/vuex-store/tools.module.js* erstellen und der Datei den folgenden Code hinzufügen.

```js
const initialState = {
    scales: "",
    currentScale: ""
};

export const state = { ...initialState };

export const mutations = {
    scales (state, scales) {
        state.scales = scales
    },
    currentScale (state, currentScale) {
        state.currentScale = currentScale
    }
};

const getters = {
    scales (state) {
        return state.scales
    },
    currentScale (state) {
        return state.currentScale
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
```


### BackBoneVueRenderer hinzufügen
Datei *modules/vue-example/renderer/BackboneVueRenderer.js* erstellen und der Datei den folgenden Code hinzufügen.

```js
const Vue = require('vue').default;
import store from '../vuex-store';

const VueComponentView = Backbone.View.extend({

    /*
        Apply the adapter pattern:
        It's a popular refactoring pattern to combine two interfaces together without
        messing with the source code too much.
    */

    constructor(component, htmlElement, defaultArgs) {
        this.component = component;
        this.htmlElement = htmlElement;
        this.defaultArgs = defaultArgs;
        this.afterRender();
    },

    afterRender() {
        const el =  $(this.htmlElement).get(0);
        new Vue({
            el,
            store: store,
            render: (h) => {
                if (this.defaultArgs) {
                    return h(this.component, { attrs: this.defaultArgs });
                } else {
                    return h(this.component);
                }
            }
        })
    }
});

export default VueComponentView;

```


### Vue Komponente erstellen
Datei *modules/vue-example/vue-files/ScaleSwitcher.vue* erstellen und der Datei den folgenden Code hinzufügen.

```
<template>
    <div class="content">
        <p>{{ title }}</p>
        <select class="form-control input-sm scale-switcher"
                v-model="currentScale">
            <option v-for="option in this.$store.getters.scales" v-bind:value="option">
                {{ option }}
            </option>
        </select>
        <br/>
    </div>
</template>

<script>
    export default {
        data: function () {
            return {
                title: this.$attrs["title"],
            }
        },
        created: function () {
            const myBus = _.extend({}, Backbone.Events);
            myBus.listenTo(Radio.channel("MapView"), {
                "changedOptions": this.storeCurrentScale
            });
            this.$store.commit('scales', Radio.request("MapView", "getScales"));
            this.$store.commit('currentScale', Radio.request("MapView", "getOptions").scale);
        },
        computed: {
            // Two-way Computed Property
            currentScale: {
                get () {
                    return this.$store.getters.currentScale
                },
                set (value) {
                    const scale = parseInt(event.target.value);
                    this.$store.commit('currentScale', scale);
                    Radio.trigger("MapView", "setScale", scale);
                }
            }
        },
        methods: {
            storeCurrentScale(value) {
                this.$store.commit('currentScale', value.scale);
            }
        }
    }
</script>

<style scoped>
    .content {
        padding: 5px;
    }
    p {
        text-align: left;
        font-size: 0.8em;
    }
    .scale-switcher {
        border: 1px solid red;
    }
</style>
```


### In der Scale-Switcher-Menu View die Erstellung der Vue Komponente initialisieren
Datei *modules/vue-example/scale-switcher-menu/view.js*
Die render() Methode wie folgt erweitern und die entsprechenden Importe hinzufügen

```js
    import BackboneVueRenderer from "../renderer/BackboneVueRenderer";
    import VueComponent from "../vue-files/ScaleSwitcher.vue";
    
    ...
    
    render: function (model, value) {
        if (value) {
            // Render the Vue and add it to a html element
            new BackboneVueRenderer(
                VueComponent, document.getElementsByClassName("win-body")[0], {title: 'Adjust the map scale'}
            );
        }
    }
    
    ...
```


### Tool in der config.json konfigurieren
Um das Tool in einem Portal zu verwenden, muss dies in der config.json konfiguriert werden.

```json
      "tools":
      {
        "name": "Werkzeuge",
        "glyphicon": "glyphicon-wrench",
        "children": {
          "scale":
          {
            "name": "Scale Switcher",
            "glyphicon": "glyphicon-resize-full"
          },
          ... // weitere Tools
        }
      }
```
