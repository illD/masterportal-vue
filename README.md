Masterportal
=================
Das Masterportal ist ein Baukasten für Geo-Anwendungen im Web auf Basis von [OpenLayers](https://openlayers.org) und [Backbone.js](https://backbonejs.org). Das Masterportal ist OpenSource Software. Es ist unter der [MIT-Lizenz](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/stable/License.txt) veröffentlicht.

Das Masterportal ist ein Projekt der [Geowerkstatt Hamburg](https://www.hamburg.de/geowerkstatt/).

## für Anwender (Stable-Version)
* [Download](https://bitbucket.org/geowerkstatt-hamburg/masterportal/downloads/)
* [Quickstart für Anwender](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/stable/doc/setup.md)
* [Remote Interface](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/stable/doc/remoteInterface.md)
* [Dokumentation für Anwender](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/stable/doc/doc.md)
* [Community Board (Anwender-Forum, Issue Tracker)](https://trello.com/c/qajdXkMa/110-willkommen)

## für Entwickler
* [Dokumentation für Entwickler](doc/devdoc.md)
* [Tutorial 01: Ein neues Modul erstellen (Scale Switcher)](doc/02_tutorial_new_module_scale_switcher.md)
* [Community Board (Entwicklerforum, Issue Tracker)](https://trello.com/c/qajdXkMa/110-willkommen)
