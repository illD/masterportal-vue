
const initialState = {
    scales: "",
    currentScale: ""
};

export const state = { ...initialState };

export const mutations = {
    scales (state, scales) {
        state.scales = scales
    },
    currentScale (state, currentScale) {
        state.currentScale = currentScale
    }
};

export const actions = {
    //Example actions
    // async [FAVORITE_REMOVE](context, slug) {
    //     const { data } = await FavoriteService.remove(slug);
    //     // Update list as well. This allows us to favorite an article in the Home view.
    //     context.commit(UPDATE_ARTICLE_IN_LIST, data.article, { root: true });
    //     context.commit(SET_ARTICLE, data.article);
    // },
    // [ARTICLE_PUBLISH]({ state }) {
    //     return ArticlesService.create(state.article);
    // },
};

const getters = {
    scales (state) {
        return state.scales
    },
    currentScale (state) {
        return state.currentScale
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
