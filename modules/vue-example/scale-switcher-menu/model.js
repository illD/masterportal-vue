import Tool from "../../core/modelList/tool/model";
import store from '../vuex-store';

const ScaleModel = Tool.extend({
    defaults: _.extend({}, Tool.prototype.defaults, {
        glyphicon: "glyphicon-resize-full",
        renderToWindow: true
    }),
    initialize: function () {
        this.superInitialize();
    }
});

export default ScaleModel;
