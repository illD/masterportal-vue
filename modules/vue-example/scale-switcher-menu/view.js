import BackboneVueRenderer from "../renderer/BackboneVueRenderer";
import VueComponent from "../vue-files/ScaleSwitcher.vue";

const ScaleView = Backbone.View.extend({
    initialize: function () {
        this.listenTo(this.model, {
            "change:isActive": this.render
        });
        if (this.model.get("isActive") === true) {
            this.render(this.model, true);
        }
    },

    render: function (model, value) {
        if (value) {
            // Render the Vue and add it to a html element
            new BackboneVueRenderer(
                VueComponent, document.getElementsByClassName("win-body")[0], {title: 'Adjust the map scale'}
            );
        }
    }
});

export default ScaleView;
