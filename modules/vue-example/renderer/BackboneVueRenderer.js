const Vue = require('vue').default;
import store from '../vuex-store';

const VueComponentView = Backbone.View.extend({

    /*
        Apply the adapter pattern:
        It's a popular refactoring pattern to combine two interfaces together without
        messing with the source code too much.
    */

    constructor(component, htmlElement, defaultArgs) {
        this.component = component;
        this.htmlElement = htmlElement;
        this.defaultArgs = defaultArgs;
        this.afterRender();
    },

    afterRender() {
        const el =  $(this.htmlElement).get(0);
        new Vue({
            el,
            store: store,
            render: (h) => {
                if (this.defaultArgs) {
                    return h(this.component, { attrs: this.defaultArgs });
                } else {
                    return h(this.component);
                }
            }
        })
    }
});

export default VueComponentView;

